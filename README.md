# defragment

Tool for defragmenting ogg files saved as YY-MM-DD-show_name.ogg

## Why??

Liquidsoap/Airtime saving shows in ogg format timestamped with YY-MM-DD. If the DJ's connection goes out, the shows get chopped up into several files. 

The shows are saved with YY-MM-DD-show_name.ogg, so this processes those filenames, combines them based on date and show name, saves them to an output directory, and moves the original files to a backup dir.

It's usually run from a cron job in the middle of the night.

## Requirements
* vorbis-tools (ogg123, oggenc) version 1.4.0-6ubuntu1 or later, with this patch:
https://bugzilla.redhat.com/show_bug.cgi?id=1185558
or this one:
https://git.xiph.org/?p=vorbis-tools.git;a=commit;h=514116d7bea89dad9f1deb7617b2277b5e9115cd

* mutagen library for python3

## Installation

	lein bin

Create the installation file.

## Usage

  java -jar defragmenter.jar config.edn

## Options

TODO: document the config file options

```clojure

{:backup-dir "/home/backups"
 :cmd-path "/bin/cat"
 :db-path "/home/streams/defragment-cache.nip"
 :duration-path "/usr/local/bin/duration.py"
 :image-base-url "https://foo.com/archives/images/"
 :in-oggs-path "/home/streams"
 :out-commands-file "/tmp/get-me"
 :out-oggs-path "/home/imports"
 :python-path "/usr/bin/python3"
 :rss-base-url "http://foo.com,/archives/imports/"
 :rss-out-file "/home/streams/archives.rss"
 :rss-self-url "http://foo.com/archives.rss"
 :run-concatenate? true
 :run-feed? true }
```

## Examples

See example.sh for a sample runner

### Bugs

Actually, none at the moment, I'm sure some will crop up at some point.

Currently in the process of being refactored

Should be split up into two programs: one for feedmaking, one for defragmenting

## License

Copyright © 2014-2023 ken restivo <ken@restivo.org>

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
