(defproject defragment "0.1.21"
  :description "Ogg file defragmenter"
  :url "https://gitlab.com/kenrestivo/defragment"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :plugins [[lein-bin "0.3.4"]]

  :dependencies [
                 [cheshire "5.5.0"]
                 [clj-http "2.0.0"]
                 [clj-time "0.11.0"]
                 [com.taoensso/timbre "4.1.4"]
                 [me.raynes/conch "0.8.0"]
                 [org.adamb/jvorbiscomment "1.0"]
                 [org.gagravarr/vorbis-java-core "0.8"]
                 [org.gagravarr/vorbis-java-tools "0.8"]
                 [org.clojure/clojure "1.7.0"]
                 [org.clojure/data.xml "0.0.8"]
                 [org.clojure/tools.trace "0.7.8"]
                 [useful "0.8.8"]
                 [utilza "0.1.72"]
                 ]
  :main  defragment.core
  :uberjar-name "defragment.jar" ;; must be here because bin uses it
  :bin {:name "defragment"}
  ;; needed for this wacky jorbiscomment thing
  :repositories [["kens" "https://restivo.org/mvn"]]
  :profiles {:uberjar {:aot :all
                       :dependencies []}
             :dev {:dependencies [[org.timmc/handy "1.7.0" :exclusions [[org.clojure/clojure]]]]}})
