#!/usr/bin/python3

import mutagen
import sys

# in production:
# /usr/lib/airtime/airtime_virtualenv/bin/python
# but we are using the system python3 because yolo

file_info = mutagen.File(sys.argv[1], easy=True)
print(getattr(file_info.info, "length", 0))

