(ns defragment.duration
  (:import [org.gagravarr.vorbis VorbisFile]
           [org.gagravarr.ogg OggFile]
           [org.gagravarr.ogg.audio OggAudioStatistics]
           )
  (:require
   [utilza.repl :as urepl]
   [utilza.repl :as urepl]
   [clojure.java.io :as jio]
   ))



(defn seconds
  "Takes file path.
  Returns the duration in seconds as a Double"
  [fname]
  (let [ogg (OggFile. (jio/input-stream fname))
        vf (VorbisFile. ogg)
        stats (OggAudioStatistics. vf vf)]
    (.calculate stats) ;; fucking side effects
    (.getDurationSeconds stats)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment


  ;; TODO: put a sample ogg file in the codebase or elsewhere on the internets and run a test
  
  (seconds "/mnt/sdcard/tmp/brat.ogg")


  
  )




