(ns defragment.utils
  (:import [adamb.vorbis VorbisCommentHeader VorbisIO CommentField CommentUpdater])
  (:require [me.raynes.conch :refer [programs with-programs let-programs] :as csh]
            [me.raynes.conch.low-level :as sh]
            [utilza.repl :as urepl]
            [clojure.tools.trace :as trace]
            [taoensso.timbre.appenders.core :as appenders]
            [useful.map :as um]
            [utilza.java :as ujava]
            [clojure.edn :as edn]
            [clojure.java.io :as jio]
            [utilza.file :as file]
            [utilza.misc :as umisc]
            [taoensso.timbre :as log]
            [clojure.string :as st]))



(defn comments->vec
  "Takes array of CommentFields and translates to a proper Clojure vector"
  [hs]
  (for [^CommentField h hs]
    [(-> h .name st/lower-case keyword) (.value h)]))

(defn header->map
  [^VorbisCommentHeader h]
  (->> h
       .fields
       comments->vec
       (into {})))


(defmacro spewer
  "Executes body within a try/catch, spews the result to /tmp/foo.edn,
     and logs any errors to timbre"
  [& body]
  `(try
     (spit "/tmp/foo.edn" "")
     (let [res# ~@body]
       (urepl/massive-spew "/tmp/foo.edn" res#))
     (catch Exception e#
       (log/error e#)
       :error)))


(defn setup!
  "The Words Before All Else"
  []
  ;; IMPORTANT: This bare exec is here to dothis FIRST before running anything, at compile time
  (log/merge-config! {:output-fn (partial log/default-output-fn {:stacktrace-fonts {}})
                      :level :info
                      :appenders {:println nil ;;(appenders/println-appender {:enabled? false})
                                  :spit (appenders/spit-appender
                                         {:fname "defragment.log4j"})}})


  ;; IMPORTANT: enables the very very awesome use of clojure.tools.trace/trace-vars , etc
  ;; and logs the output of those traces to whatever is configured for timbre at the moment!
  (alter-var-root #'clojure.tools.trace/tracer (fn [_]
                                                 (fn [name value]
                                                   (log/debug name value)))))


(defn slurp-and-decode
  "Read and deserialize the config"
  [config-path]
  (->> config-path
       slurp
       edn/read-string))


(defn revision-info
  "Utility for determing the program's revision."
  []
  (let [{:keys [version revision]} (ujava/get-project-properties "defragment" "defragment")]
    (format "Version: %s, Revision %s" version revision)))
