(ns defragment.core-test
  (:require [clojure.test :refer :all]
            [taoensso.timbre :as log]
            [utilza.repl :as urepl]
            [defragment.utils :as u]
            [clojure.edn :as edn]
            [defragment.core :refer :all]))


(deftest fileglobbing
  (is (= (->> "test_data/unglobbed.edn"
              u/slurp-and-decode
              glob-all)
         (u/slurp-and-decode "test_data/globbed.edn"))))

(comment

  (->> "resources/test-config.edn"
       process-config
       :in-oggs-path
       get-oggs
       (map parse)
       u/spewer)

  ;;; raw shows
  (->> "resources/test-config.edn"
       process-config
       :in-oggs-path
       get-oggs
       (map parse)
       (sort-and-partition :show )
       u/spewer)

  (->> "resources/test-config.edn"
       process-config
       :in-oggs-path
       get-oggs
       (map parse)
       prepare-shows
       u/spewer)
  
  ;; everything
  (->> "resources/test-config.edn"
       process-config
       :in-oggs-path
       get-oggs
       (map parse)
       glob-all
       u/spewer)
  
  

  (->> "resources/test-config.edn"
       process-config
       run-all)

  ;; only those without shows
  (->> "resources/test-config.edn"
       process-config
       :in-oggs-path
       get-oggs
       (map parse)
       without-shows
       u/spewer) 

  )


(comment
  
  (->> "resources/test-config.edn"
       process-config
       :in-oggs-path
       get-oggs
       (map parse)
       (concat-shows! "/mnt/sdcard/tmp"))

  )

(comment
  (<  20141011231803 20141011225718)
  (< 20141011225757  20141011225809)

  )


(comment

  
  (->> "/mnt/sdcard/tmp/2014-10-11-test_in_the_dark_illegal_chars.ogg"
       jio/as-file
       VorbisIO/readComments
       .fields
       header->vec)


  


  
  
  (urepl/hjall *1)


  (->> "/mnt/sdcard/tmp/2014-10-02-.ogg"
       jio/as-file
       VorbisIO/readComments
       .fields)

  
  (->> "/mnt/sdcard/tmp/2014-10-02-.ogg"
       jio/as-file
       VorbisIO/readComments
       .fields
       (filter bad-title))


  
  (->> "/mnt/sdcard/tmp/2014-10-02-.ogg"
       jio/as-file
       VorbisIO/readComments
       .fields
       (map #(.name %)))
  
  (def foo *1)

  

  (->> "/mnt/sdcard/tmp/2014-09-28-.ogg"
       jio/as-file
       VorbisIO/readComments
       .fields
       (filter bad-title))

  
  (fix-in-place "/mnt/sdcard/tmp/2014-10-02-.ogg" (title-fixer "2014-10-02"))

  ;; for testing
  (fix-in-place "/mnt/sdcard/tmp/2014-10-02-.ogg" 
                (reify CommentUpdater
                  (updateComments [this comments]
                    (let [fields (.fields comments)]
                      (.add fields (CommentField. "ALBUM" "this is a Test Artist in Album yeah."))
                      true))))

  (fix-in-place "/mnt/sdcard/tmp/2014-10-02-.ogg" (album-mover))

  )


(comment

  (log/set-level! :trace)


  (gen-outfile-name "/tmp" "fuck" "2023-01-28" )


  (gen-command-line {:name "SOCARRAT_SOUNDS", :date "2023-01-27", :filenames '("/home/streams/2023-01-27-19_04_41-SOCARRAT_SOUNDS.ogg" "/home/streams/2023-01-27-22_56_55-SOCARRAT_SOUNDS.ogg")}
                    (gen-outfile-name "/tmp" "fuck" "2023-01-28" ))


  (concatenate! "/bin/cat"
                "/tmp/2023-01-28-02_26_51-SOCARRAT_SOUNDS.ogg"
                "/tmp/get-me"
                {:name "SOCARRAT_SOUNDS",
                 :date "2023-01-27",
                 :filenames '("/mnt/sdcard/tmp/2023-01-28-02_26_51-SOCARRAT_SOUNDS.ogg")})
  
  (execute-all! {:cmd-path "/bin/cat"
                 :out-oggs-path "/tmp"
                 :out-commands-file "/tmp/get-me"
                 :backup-dir "/tmp/backups"}
                
                {:name "SOCARRAT_SOUNDS",
                 :date "2023-01-27",
                 :filenames '("/mnt/sdcard/tmp/2023-01-28-02_26_51-SOCARRAT_SOUNDS.ogg")})

  (fix-comments! "/tmp/2023-01-28-02_26_51-SOCARRAT_SOUNDS.ogg"
                 "SOCARRAT_SOUNDS"
                 "2023-01-27"
                 ["/mnt/sdcard/tmp/2023-01-28-02_26_51-SOCARRAT_SOUNDS.ogg"])


  (-> "/mnt/sdcard/tmp/2023-01-28-02_26_51-SOCARRAT_SOUNDS.ogg"
      jio/file
      VorbisIO/readComments
      .fields)
  
  
  (transfer-comments! 
   "/mnt/sdcard/tmp/2023-01-28-02_26_51-SOCARRAT_SOUNDS.ogg"
   "/tmp/2023-01-28-02_26_51-SOCARRAT_SOUNDS.ogg")

  

  )

(comment

  (do
    (setup!)
    (log/set-level! :trace))

  
  (try (->> "/home/cust/spaz/configs/defragment-configs/test-concat.edn"
            process-config
            run-all!)
       (catch Exception e
         (log/error e)))


  (clojure.tools.trace/trace-vars parse)



  (let [in-oggs-path "/tmp/mem/streams"]
    (->> in-oggs-path
         get-file-list
         glob-all
         u/spewer))
  

  )
