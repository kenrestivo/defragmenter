(ns defragment.feed-test
  (:require [clojure.test :refer :all]
            [utilza.repl :as urepl]
            [clojure.edn :as edn]
            [defragment.db :as db]
            [defragment.utils :as u]
            [taoensso.timbre :as log]
            [defragment.feed :refer :all]))




(deftest basic-feedifying
  (is (= (->> "test_data/feed-files.edn"
              u/slurp-and-decode
              (xml-feedify "foo"))
         (->> "test_data/feed.rss"
              slurp))))


(deftest date-unfucking
  (is (= (unfuck-date
          "Tue, 09 Sep 2014 00:00:00 +0000")
         "2014-09-09")))

(comment

  ;; TODO: get the files, get list of shows in it, get all the feeds for those shows
  

  (log/set-level! :trace)

  (log/set-level! :info)
  
  (log/info "testing")
  
  
  (urepl/massive-spew "/tmp/foo.edn" *1)

  (-> "resources/test-config.edn"
      slurp
      edn/read-string
      make-feed!)
  
  (def db (atom []))

  (do ;; so as not to freako out emacs
    (reset! db [])
    (db/read-data! db "/mnt/sdcard/tmp/defragment-cache.nip")
    true)

  (->> @db
       (filter #(-> % :basename (.contains "SOC")))
       (sort-by date-attempt)
       u/spewer)

  (->> @db
       #_(filter #(-> % :basename (.contains "SOC")))
       (sort-by date-attempt)
       (xml-feedify "foo")
       (spit "/tmp/foo.rss"))


  ;; 108 fucked up files with no artist tag!
  (->> @db
       (filter :show)
       (remove :artist)
       (sort-by date-attempt)
       u/spewer)

  ;; 9 fucked up files with no title tag!
  (->> @db
       (filter :show)
       (remove :title)
       u/spewer)

  
  )

(comment

  (->> @db
       (sort-by date-attempt)
       reverse
       (take 10)
       u/spewer)

  

  )
